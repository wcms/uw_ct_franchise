<?php

/**
 * @file
 * uw_ct_franchise.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_franchise_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-manager-vocabularies_allergens:admin/structure/taxonomy/allergens.
  $menu_links['menu-site-manager-vocabularies_allergens:admin/structure/taxonomy/allergens'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/allergens',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Allergens',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_allergens:admin/structure/taxonomy/allergens',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_cuisines:admin/structure/taxonomy/cuisines.
  $menu_links['menu-site-manager-vocabularies_cuisines:admin/structure/taxonomy/cuisines'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/cuisines',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Cuisines',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_cuisines:admin/structure/taxonomy/cuisines',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_dietary-types:admin/structure/taxonomy/uw_fs_dietary_types.
  $menu_links['menu-site-manager-vocabularies_dietary-types:admin/structure/taxonomy/uw_fs_dietary_types'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_fs_dietary_types',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Dietary types',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_dietary-types:admin/structure/taxonomy/uw_fs_dietary_types',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_food-outlet-types:admin/structure/taxonomy/uw_food_outlet_types.
  $menu_links['menu-site-manager-vocabularies_food-outlet-types:admin/structure/taxonomy/uw_food_outlet_types'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_food_outlet_types',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Food outlet types',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_food-outlet-types:admin/structure/taxonomy/uw_food_outlet_types',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_meal-types:admin/structure/taxonomy/uw_fs_meal_type.
  $menu_links['menu-site-manager-vocabularies_meal-types:admin/structure/taxonomy/uw_fs_meal_type'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_fs_meal_type',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Meal types',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_meal-types:admin/structure/taxonomy/uw_fs_meal_type',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_payment-types:admin/structure/taxonomy/uw_payment_type.
  $menu_links['menu-site-manager-vocabularies_payment-types:admin/structure/taxonomy/uw_payment_type'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uw_payment_type',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Payment types',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_payment-types:admin/structure/taxonomy/uw_payment_type',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Allergens');
  t('Cuisines');
  t('Dietary types');
  t('Food outlet types');
  t('Meal types');
  t('Payment types');

  return $menu_links;
}
