<?php

/**
 * @file
 * uw_ct_franchise.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_franchise_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_ct_franchise content'.
  $permissions['create uw_ct_franchise content'] = array(
    'name' => 'create uw_ct_franchise content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_ct_franchise content'.
  $permissions['delete any uw_ct_franchise content'] = array(
    'name' => 'delete any uw_ct_franchise content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_ct_franchise content'.
  $permissions['delete own uw_ct_franchise content'] = array(
    'name' => 'delete own uw_ct_franchise content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_ct_franchise content'.
  $permissions['edit any uw_ct_franchise content'] = array(
    'name' => 'edit any uw_ct_franchise content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_ct_franchise content'.
  $permissions['edit own uw_ct_franchise content'] = array(
    'name' => 'edit own uw_ct_franchise content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_ct_franchise revision log entry'.
  $permissions['enter uw_ct_franchise revision log entry'] = array(
    'name' => 'enter uw_ct_franchise revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_franchise authored by option'.
  $permissions['override uw_ct_franchise authored by option'] = array(
    'name' => 'override uw_ct_franchise authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_franchise authored on option'.
  $permissions['override uw_ct_franchise authored on option'] = array(
    'name' => 'override uw_ct_franchise authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_franchise comment setting option'.
  $permissions['override uw_ct_franchise comment setting option'] = array(
    'name' => 'override uw_ct_franchise comment setting option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_franchise promote to front page option'.
  $permissions['override uw_ct_franchise promote to front page option'] = array(
    'name' => 'override uw_ct_franchise promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_franchise published option'.
  $permissions['override uw_ct_franchise published option'] = array(
    'name' => 'override uw_ct_franchise published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_franchise revision option'.
  $permissions['override uw_ct_franchise revision option'] = array(
    'name' => 'override uw_ct_franchise revision option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_franchise sticky option'.
  $permissions['override uw_ct_franchise sticky option'] = array(
    'name' => 'override uw_ct_franchise sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  return $permissions;
}
