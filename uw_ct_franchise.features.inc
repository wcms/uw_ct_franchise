<?php

/**
 * @file
 * uw_ct_franchise.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_franchise_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_franchise_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_franchise_node_info() {
  $items = array(
    'uw_ct_franchise' => array(
      'name' => t('Franchise menu'),
      'base' => 'node_content',
      'description' => t('Content type for franchise menu items.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function uw_ct_franchise_paragraphs_info() {
  $items = array(
    'franchise_addons' => array(
      'name' => 'Franchise Add-Ons',
      'bundle' => 'franchise_addons',
      'locked' => '1',
    ),
    'franchise_combos' => array(
      'name' => 'Franchise Combos',
      'bundle' => 'franchise_combos',
      'locked' => '1',
    ),
    'franchise_individual' => array(
      'name' => 'Franchise Individual Items',
      'bundle' => 'franchise_individual',
      'locked' => '1',
    ),
  );
  return $items;
}
