<?php

/**
 * @file
 * uw_ct_franchise.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_franchise_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_combo_other|paragraphs_item|franchise_combos|form';
  $field_group->group_name = 'group_combo_other';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'franchise_combos';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Other Combo Items',
    'weight' => '3',
    'children' => array(
      0 => 'field_combo_other',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-combo-other field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_combo_other|paragraphs_item|franchise_combos|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Other Combo Items');

  return $field_groups;
}
