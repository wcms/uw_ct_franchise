<?php

/**
 * @file
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div class="content_node franchise-logo"<?php print $content_attributes; ?>>
    <?php print render($content['field_franchise_logo']); ?>
  </div>
  <div class="franchise-menu-list">
    <?php
    if (!empty($content['field_combos'])) {
      foreach ($content['field_combos'] as $key => $combo): ?>
        <?php if (is_int($key)): ?>
          <div class="franchise-menu-list-item franchise-menu-combo">
            <?php print render($content['field_combos'][$key]); ?>
          </div>
        <?php endif; ?>
      <?php endforeach;
    }
    if (!empty($content['field_individual_items'])) {
      foreach ($content['field_individual_items'] as $key => $ind): ?>
        <?php if (is_int($key)): ?>
          <div class="franchise-menu-list-item franchise-menu-ind">
             <?php print render($content['field_individual_items'][$key]); ?>
           </div>
        <?php endif; ?>
      <?php endforeach;
    }
    if (!empty($content['field_addons'])) {
      foreach ($content['field_addons'] as $key => $addon): ?>
        <?php if (is_int($key)): ?>
          <div class="franchise-menu-list-item franchise-menu-addon">
            <?php print render($content['field_addons'][$key]); ?>
          </div>
        <?php endif; ?>
      <?php endforeach;
    }
  ?>
  </div>
</div>
