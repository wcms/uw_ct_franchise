<?php

/**
 * @file
 * uw_ct_franchise.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_franchise_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_food_outlet_page';
  $context->description = 'Displays franchise menu under food outlet.';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/125' => 'node/125',
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'quicktabs-franchise_tab_block' => array(
          'module' => 'quicktabs',
          'delta' => 'franchise_tab_block',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays franchise menu under food outlet.');
  $export['uw_food_outlet_page'] = $context;

  return $export;
}
